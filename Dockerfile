FROM node:latest
RUN git clone https://gitlab.com/kushwah8815311746/flipkart-clone.git
WORKDIR flipkart-clone/frontend/
RUN npm install
ENTRYPOINT npm start
